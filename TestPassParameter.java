
public class TestPassParameter {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DigitalVideoDisc jungleDVD = new DigitalVideoDisc("Jungle");
		DigitalVideoDisc cinderellaDVD = new DigitalVideoDisc("Cinderella");
		
		jungleDVD=swap(cinderellaDVD,cinderellaDVD=jungleDVD);
		System.out.println("jungle dvd title: " + jungleDVD.getTitle());
		System.out.println("ciderella dvd title: " + cinderellaDVD.getTitle());
		
		changeTitle(jungleDVD, cinderellaDVD.getTitle());
		System.out.println("jungle dvd title: " + jungleDVD.getTitle());
	}
	
	/*public static void swap(DigitalVideoDisc o1,DigitalVideoDisc o2) {
		Object tmp = o1;
		o1=o2;
		o2=tmp;
	}*/
	public static DigitalVideoDisc swap(DigitalVideoDisc o1,DigitalVideoDisc o2) {
		return o1;
	}
	public static void changeTitle(DigitalVideoDisc dvd,String title) {
		String oldTitle = dvd.getTitle();
		dvd.setTitle(title);
		dvd = new DigitalVideoDisc(oldTitle);
	}
	
	}